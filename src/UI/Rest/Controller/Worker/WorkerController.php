<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\Worker;

use App\Application\UseCase\Worker\Request\Create;
use App\Application\UseCase\Worker\Request\GetWorker;
use App\Application\UseCase\Worker\Request\GetWorkers;
use App\Application\UseCase\Worker\Request\Update;
use App\Domain\Worker\Model\Worker;
use App\Domain\Worker\ValueObject\WorkerId;
use App\Infrastructure\Common\Exception\Form\FormException;
use App\UI\Rest\Controller\AbstractBusController;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

/**
 * Class WorkerController
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\UI\Rest\Controller\Worker
 */
class WorkerController extends AbstractBusController
{
    /**
     * Retrieves the collection of Worker resources.
     *
     * @SWG\Get(
     *     summary="Retrieves the collection of Worker resources.",
     *     description="Retrieves the collection of Worker resources.",
     *     tags={"Worker"},
     *     @SWG\Response(
     *          response="200",
     *          description="Worker collection response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(type=App\Domain\Worker\Model\Worker::class)
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     * )
     * @Nelmio\Security(name="Bearer")
     */
    public function list()
    {
        return $this->ask(new GetWorkers());
    }

    /**
     * Retrieves a Worker resource.
     *
     * @param string $uuid
     * @return Worker
     *
     * @SWG\Get(
     *     summary="Retrieves a Worker resource.",
     *     description="Retrieves a Worker resource.",
     *     tags={"Worker"},
     *     @SWG\Response(
     *          response="200",
     *          description="Worker resource response",
     *          @Nelmio\Model(type=App\Domain\Worker\Model\Worker::class)
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     */
    public function show(string $uuid): Worker
    {
        return $this->ask(new GetWorker($uuid));
    }

    /**
     * @SWG\Post(
     *     summary="Create a Worker resource.",
     *     description="Create a Worker resource.",
     *     tags={"Worker"},
     *     @SWG\Response(
     *          response="201",
     *          description="Worker resource created"
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="409",
     *          description="Already exist"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @Rest\RequestParam(name="email", strict=false, default="", description="Unique email")
     * @Rest\RequestParam(name="first_name", strict=false, default="", description="First name")
     * @Rest\RequestParam(name="last_name", strict=false, default="", description="Last name")
     * @Rest\RequestParam(name="password", strict=false, default="", description="Plain password")
     *
     * @param ParamFetcherInterface $fetcher
     * @return View|FormInterface|JsonResponse
     */
    public function create(ParamFetcherInterface $fetcher)
    {
        try {
            /** @var Worker $worker */
            $worker = $this->handle(
                new Create(
                    new WorkerId(),
                    $fetcher->get('email'),
                    $fetcher->get('first_name'),
                    $fetcher->get('last_name'),
                    $fetcher->get('password')
                )
            );

            return new JsonResponse(null, Response::HTTP_CREATED);
        } catch (FormException $e) {
            return $e->getForm();
        } catch (UniqueConstraintViolationException $e) {
            throw new ConflictHttpException('worker.exception.already_exist');
        }
    }

    /**
     * @SWG\Patch(
     *     summary="Update a Worker resource.",
     *     description="Update a Worker resource.",
     *     tags={"Worker"},
     *     @SWG\Response(
     *          response="201",
     *          description="Worker resource created"
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="409",
     *          description="Already exist"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @Rest\RequestParam(name="first_name", strict=false, default="", description="First name")
     * @Rest\RequestParam(name="last_name", strict=false, default="", description="Last name")
     *
     * @param ParamFetcherInterface $fetcher
     * @param string $uuid
     * @return View|FormInterface
     */
    public function update(ParamFetcherInterface $fetcher, string $uuid)
    {
        try {
            $worker = $this->handle(
                new Update(
                    new WorkerId($uuid),
                    $fetcher->get('first_name'),
                    $fetcher->get('last_name')
                )
            );

            return null;
        } catch (FormException $e) {
            return $e->getForm();
        }
    }
}
