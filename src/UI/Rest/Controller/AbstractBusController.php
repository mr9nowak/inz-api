<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller;

use League\Tactician\CommandBus;

/**
 * Class AbstractBusController
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\UI\Rest\Controller
 */
abstract class AbstractBusController extends AbstractController
{
    /**
     * @var CommandBus
     */
    private $bus;

    /**
     * @var CommandBus
     */
    private $queryBus;

    /**
     * AbstractBusController constructor.
     *
     * @param CommandBus $bus
     * @param CommandBus $queryBus
     */
    public function __construct(CommandBus $bus, CommandBus $queryBus)
    {
        $this->bus = $bus;
        $this->queryBus = $queryBus;
    }

    /**
     * @param object $commandRequest
     * @return mixed
     */
    public function handle($commandRequest)
    {
        return $this->bus->handle($commandRequest);
    }

    /**
     * @param object $commandRequest
     * @return mixed
     */
    public function ask($commandRequest)
    {
        return $this->queryBus->handle($commandRequest);
    }
}
