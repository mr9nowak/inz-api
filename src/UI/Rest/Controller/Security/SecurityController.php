<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\Security;

use App\Application\UseCase\Security\Request\Login;
use App\Application\UseCase\Worker\Request\GetWorker;
use App\UI\Rest\Controller\AbstractBusController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;

/**
 * Class SecurityController
 *
 * @package App\UI\Rest\Controller\Security
 */
class SecurityController extends AbstractBusController
{
    /**
     * Request user credentials.
     *
     * @SWG\Post(
     *     summary="Request user credentials.",
     *     description="Request user credentials.",
     *     tags={"Auth"},
     *     @SWG\Response(
     *          response="200",
     *          description="User credentials"
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     )
     * )
     *
     * @Rest\RequestParam(name="_username", description="Unique username identifier")
     * @Rest\RequestParam(name="_password", description="Unique plain password")
     *
     * @param ParamFetcherInterface $fetcher
     * @return array
     */
    public function loginAction(ParamFetcherInterface $fetcher): array
    {
        return [
            'token' => $this->handle(
                new Login(
                    $fetcher->get('_username'),
                    $fetcher->get('_password')
                )
            ),
        ];
    }

    /**
     * Retrieves the data of the currently logged in user.
     *
     * @SWG\Get(
     *     summary="Retrieves the data of the currently logged in user.",
     *     description="Retrieves the data of the currently logged in user.",
     *     tags={"Me"},
     *     @SWG\Response(
     *          response="200",
     *          description="Worker resource response"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     */
    public function meAction()
    {
        return $this->ask(new GetWorker($this->getUser()->id()));
    }
}
