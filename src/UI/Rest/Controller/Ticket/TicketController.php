<?php

declare(strict_types=1);

namespace App\UI\Rest\Controller\Ticket;

use App\Application\UseCase\Ticket\Request\AssignWorker;
use App\Application\UseCase\Ticket\Request\Close;
use App\Application\UseCase\Ticket\Request\Create;
use App\Application\UseCase\Ticket\Request\Estimate;
use App\Application\UseCase\Ticket\Request\GetTicket;
use App\Application\UseCase\Ticket\Request\GetTickets;
use App\Application\UseCase\Ticket\Request\Spend;
use App\Domain\Ticket\Model\Ticket;
use App\Domain\Ticket\ValueObject\TicketId;
use App\Domain\Worker\ValueObject\WorkerId;
use App\Infrastructure\Common\Exception\Form\FormException;
use App\UI\Rest\Controller\AbstractBusController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Swagger\Annotations as SWG;
use Symfony\Component\Form\FormInterface;

/**
 * Class TicketController
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\UI\Rest\Controller\Ticket
 */
class TicketController extends AbstractBusController
{
    /**
     * Retrieves the collection of Ticket resources.
     *
     * @return array
     *
     * @SWG\Get(
     *     summary="Retrieves the collection of Ticket resources.",
     *     description="Retrieves the collection of Ticket resources.",
     *     tags={"Ticket"},
     *     @SWG\Response(
     *          response="200",
     *          description="Ticket collection response",
     *          @SWG\Schema(
     *              @SWG\Items(
     *                  ref=@Nelmio\Model(type=App\Domain\Ticket\Model\Ticket::class)
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     */
    public function list(): array
    {
        return $this->ask(new GetTickets());
    }

    /**
     * Retrieves a Ticket resource.
     *
     * @param string $uuid
     * @return Ticket
     *
     * @SWG\Get(
     *     summary="Retrieves a Ticket resource.",
     *     description="Retrieves a Ticket resource.",
     *     tags={"Ticket"},
     *     @SWG\Response(
     *          response="200",
     *          description="Ticket resource response",
     *          @Nelmio\Model(type=App\Domain\Ticket\Model\Ticket::class)
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     */
    public function show(string $uuid): Ticket
    {
        return $this->ask(new GetTicket($uuid));
    }

    /**
     * Create a Ticket resource.
     *
     * @param ParamFetcherInterface $fetcher
     * @return FormInterface|null
     *
     * @Rest\RequestParam(name="email", strict=false, default="")
     * @Rest\RequestParam(name="content", strict=false, default="")
     * @Rest\RequestParam(name="answer1", strict=false, default="")
     * @Rest\RequestParam(name="answer2", strict=false, default="")
     * @Rest\RequestParam(name="answer3", strict=false, default="")
     * @Rest\RequestParam(name="answer4", strict=false, default="")
     * @Rest\RequestParam(name="answer5", strict=false, default="")
     * @Rest\RequestParam(name="answer6", strict=false, default="")
     *
     * @SWG\Post(
     *     summary="Create a Ticket resource.",
     *     description="Create a Ticket resource.",
     *     tags={"Ticket"},
     *     @SWG\Response(
     *          response="201",
     *          description="Ticket resource created",
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     )
     * )
     */
    public function create(ParamFetcherInterface $fetcher)
    {
        try {
            $ticket = $this->handle(
                new Create(
                    new TicketId(),
                    $fetcher->get('email'),
                    $fetcher->get('content'),
                    $fetcher->get('answer1'),
                    $fetcher->get('answer2'),
                    $fetcher->get('answer3'),
                    $fetcher->get('answer4'),
                    $fetcher->get('answer5'),
                    $fetcher->get('answer6')
                )
            );

            return null;
        } catch (FormException $e) {
            return $e->getForm();
        }
    }



    /**
     * @SWG\Post(
     *     summary="Set estimate time of Ticket resource.",
     *     description="Set estimate time of Ticket resource.",
     *     tags={"Ticket"},
     *     @SWG\Response(
     *          response="200",
     *          description="Ticket resource response",
     *          @Nelmio\Model(type=App\Domain\Ticket\Model\Ticket::class)
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @Rest\RequestParam(name="estimate", strict=false, default="", description="Estimate time")
     *
     * @param ParamFetcherInterface $fetcher
     * @param string $uuid
     * @return View
     */
    public function estimate(ParamFetcherInterface $fetcher, string $uuid)
    {
        /** @var Ticket $ticket */
        $ticket = $this->handle(
            new Estimate(
                new TicketId($uuid),
                (float) $fetcher->get('estimate')
            )
        );

        return $this
            ->routeRedirectView(
                'api_v1_ticket_show',
                [
                    'uuid' => $ticket->uuid()->__toString()
                ]
            )
            ->setData($ticket);
    }

    /**
     * @SWG\Post(
     *     summary="Set spend time of Ticket resource.",
     *     description="Set spend time of Ticket resource.",
     *     tags={"Ticket"},
     *     @SWG\Response(
     *          response="200",
     *          description="Ticket resource response",
     *          @Nelmio\Model(type=App\Domain\Ticket\Model\Ticket::class)
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @Rest\RequestParam(name="spend", strict=false, default="", description="Spend time")
     *
     * @param ParamFetcherInterface $fetcher
     * @param string $uuid
     * @return View
     */
    public function spend(ParamFetcherInterface $fetcher, string $uuid)
    {
        /** @var Ticket $ticket */
        $ticket = $this->handle(
            new Spend(
                new TicketId($uuid),
                (float) $fetcher->get('spend')
            )
        );

        return $this
            ->routeRedirectView(
                'api_v1_ticket_show',
                [
                    'uuid' => $ticket->uuid()->__toString()
                ]
            )
            ->setData($ticket);
    }

    /**
     * @SWG\Post(
     *     summary="Close Ticket resource.",
     *     description="Close Ticket resource.",
     *     tags={"Ticket"},
     *     @SWG\Response(
     *          response="200",
     *          description="Ticket resource response",
     *          @Nelmio\Model(type=App\Domain\Ticket\Model\Ticket::class)
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @param string $uuid
     * @return View
     */
    public function close(string $uuid)
    {
        /** @var Ticket $ticket */
        $ticket = $this->handle(
            new Close(
                new TicketId($uuid)
            )
        );

        return $this
            ->routeRedirectView(
                'api_v1_ticket_show',
                [
                    'uuid' => $ticket->uuid()->__toString()
                ]
            )
            ->setData($ticket);
    }

    /**
     * @SWG\Post(
     *     summary="Assign Worker resource to Ticket resource.",
     *     description="Assign Worker resource to Ticket resource.",
     *     tags={"Ticket"},
     *     @SWG\Response(
     *          response="200",
     *          description="Ticket resource response",
     *          @Nelmio\Model(type=App\Domain\Ticket\Model\Ticket::class)
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Invalid input"
     *     ),
     *     @SWG\Response(
     *          response="401",
     *          description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found"
     *     )
     * )
     * @Nelmio\Security(name="Bearer")
     *
     * @Rest\RequestParam(name="worker", strict=false, default="", description="Worker identifier")
     *
     * @param ParamFetcherInterface $fetcher
     * @param string $uuid
     * @return View
     */
    public function assignWorker(
        ParamFetcherInterface $fetcher,
        string $uuid
    ) {
        $ticket = $this->handle(
            new AssignWorker(
                new TicketId($uuid),
                new WorkerId($fetcher->get('worker'))
            )
        );

        return $this
            ->routeRedirectView(
                'api_v1_ticket_show',
                [
                    'uuid' => $ticket->uuid()->__toString()
                ]
            )
            ->setData($ticket);
    }
}
