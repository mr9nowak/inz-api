<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Exception\Form;

use Exception;
use Symfony\Component\Form\FormInterface;

/**
 * Class FormException
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Infrastructure\Common\Exception\Form
 */
class FormException extends Exception
{
    /**
     * @var FormInterface
     */
    private $form;

    /**
     * FormException constructor.
     *
     * @param FormInterface $form
     */
    public function __construct(FormInterface $form)
    {
        parent::__construct('Form Error', 0);

        $this->form = $form;
    }

    /**
     * @return FormInterface
     */
    public function getForm(): FormInterface
    {
        return $this->form;
    }
}
