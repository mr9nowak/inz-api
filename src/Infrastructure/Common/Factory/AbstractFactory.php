<?php

declare(strict_types=1);

namespace App\Infrastructure\Common\Factory;

use App\Infrastructure\Common\Exception\Form\FormException;
use App\Infrastructure\Common\Exception\Form\FormFactoryException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Class AbstractFactory
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Infrastructure\Common\Factory
 */
abstract class AbstractFactory
{
    const CREATE = 'POST';
    const REPLACE = 'PUT';
    const UPDATE = 'PATCH';

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var string
     */
    protected $formClass;

    /**
     * AbstractFactory constructor.
     *
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param string $action
     * @param array $data
     * @param null $object
     * @return mixed
     * @throws FormException
     */
    protected function execute(string $action = self::CREATE, array $data = [], $object = null)
    {
        if (! $this->formClass) {
            throw new FormFactoryException();
        }

        $form = $this->createForm($action, $object)->submit($data, self::UPDATE !== $action);

        if (! $form->isValid()) {
            throw new FormException($form);
        }

        return $form->getData();
    }

    /**
     * @param string $action
     * @param null $object
     * @return FormInterface
     */
    private function createForm(string $action = self::CREATE, $object = null): FormInterface
    {
        return $this->formFactory->create($this->formClass, $object, [
            'method' => $action,
        ]);
    }
}
