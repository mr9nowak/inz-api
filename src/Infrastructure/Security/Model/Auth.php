<?php

declare(strict_types=1);

namespace App\Infrastructure\Security\Model;

use App\Domain\Security\ValueObject\AuthWorker;
use Symfony\Component\Security\Core\Encoder\EncoderAwareInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class Auth
 *
 * @package App\Infrastructure\Security\Model
 */
class Auth implements UserInterface, EncoderAwareInterface
{
    /**
     * @var string
     */
    private $uuid;

    /**
     * @var AuthWorker
     */
    private $authWorker;

    /**
     * Auth constructor.
     *
     * @param string $uuid
     * @param AuthWorker $authWorker
     */
    public function __construct(string $uuid, AuthWorker $authWorker)
    {
        $this->uuid = $uuid;
        $this->authWorker = $authWorker;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->uuid;
    }

    /**
     * {@inheritDoc}
     */
    public function getRoles()
    {
        return $this->authWorker->roles();
    }

    /**
     * {@inheritDoc}
     */
    public function getPassword()
    {
        return $this->authWorker->password();
    }

    /**
     * {@inheritDoc}
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function getUsername()
    {
        return $this->authWorker->username();
    }

    /**
     * {@inheritDoc}
     */
    public function eraseCredentials()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getEncoderName()
    {
        return 'harsh';
    }
}
