<?php

declare(strict_types=1);

namespace App\Infrastructure\Security\Provider;

use App\Domain\Worker\Repository\WorkerRepositoryInterface;
use App\Infrastructure\Security\Model\Auth;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class WorkerProvider
 *
 * @package App\Infrastructure\Security\Provider
 */
class WorkerProvider implements UserProviderInterface
{
    /**
     * @var WorkerRepositoryInterface
     */
    private $repository;

    /**
     * WorkerProvider constructor.
     *
     * @param WorkerRepositoryInterface $repository
     */
    public function __construct(WorkerRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritDoc}
     */
    public function loadUserByUsername($username)
    {
        $worker = $this->repository->findOneByUsername($username);

        if (! $worker) {
            throw new UsernameNotFoundException();
        }

        return new Auth($worker->uuid()->__toString(), $worker->auth());
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        return Auth::class === $class;
    }
}
