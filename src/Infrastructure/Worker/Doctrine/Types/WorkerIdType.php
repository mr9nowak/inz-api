<?php

declare(strict_types=1);

namespace App\Infrastructure\Worker\Doctrine\Types;

use App\Domain\Common\ValueObject\AggregateRootId;
use App\Domain\Worker\ValueObject\WorkerId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Ramsey\Uuid\Doctrine\UuidBinaryType;

/**
 * Class WorkerIdType
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Infrastructure\Worker\Doctrine\Types
 */
class WorkerIdType extends UuidBinaryType
{
    const WORKER_ID = 'workerId';

    /**
     * {@inheritDoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return (null === $value) ? null : WorkerId::fromBytes($value);
    }

    /**
     * {@inheritDoc}
     *
     * @param AggregateRootId|null $value
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return (null === $value) ? null : $value->bytes();
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return self::WORKER_ID;
    }
}
