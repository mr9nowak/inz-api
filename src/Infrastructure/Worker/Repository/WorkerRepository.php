<?php

declare(strict_types=1);

namespace App\Infrastructure\Worker\Repository;

use App\Domain\Worker\Exception\WorkerNotFoundException;
use App\Domain\Worker\Model\Worker;
use App\Domain\Worker\Repository\WorkerRepositoryInterface;
use App\Domain\Worker\ValueObject\WorkerId;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class WorkerRepository
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Infrastructure\Worker\Repository
 */
class WorkerRepository extends ServiceEntityRepository implements WorkerRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Worker::class);
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return parent::findAll();
    }

    /**
     * {@inheritDoc}
     */
    public function findOneByUsername(?string $username): ?Worker
    {
        return $this->createQueryBuilder('w')
            ->where('w.auth.username = :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * {@inheritDoc}
     */
    public function findOneByUuid(WorkerId $workerId): ?Worker
    {
        return $this->createQueryBuilder('worker')
            ->where('worker.uuid = :uuid')
            ->setParameter('uuid', $workerId->bytes())
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * {@inheritDoc}
     */
    public function getOneByUuid(WorkerId $workerId): Worker
    {
        $worker = $this->findOneByUuid($workerId);

        if (! $worker) {
            throw new WorkerNotFoundException();
        }

        return $worker;
    }

    /**
     * {@inheritDoc}
     */
    public function save(Worker $worker): void
    {
        $this->_em->persist($worker);
        $this->_em->flush($worker);
    }
}
