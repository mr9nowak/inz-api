<?php

declare(strict_types=1);

namespace App\Infrastructure\Worker\Factory\Form;

use App\Domain\Worker\Model\Worker;
use App\Infrastructure\Security\ValueObject\EncodedPassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class CreateType
 *
 * @package App\Infrastructure\Worker\Factory\Form
 */
class CreateType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('uuid', null, [
                'mapped' => false,
            ])
            ->add('email', EmailType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'worker.exception.email.not_blank',
                    ]),
                    new NotNull([
                        'message' => 'worker.exception.null.not_null',
                    ]),
                    new Email([
                        'message' => 'worker.exception.email.not_valid',
                    ]),
                ],
                'mapped' => false,
            ])
            ->add('first_name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'worker.exception.first_name.not_blank',
                    ]),
                ],
                'mapped' => false,
            ])
            ->add('last_name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'worker.exception.last_name.not_blank',
                    ]),
                ],
                'mapped' => false,
            ])
            ->add('password', PasswordType::class, [
                'mapped' => false
            ])
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Worker::class,
            'csrf_protection' => false,
            'empty_data' => function (FormInterface $form) {
                return Worker::create(
                    $form->get('uuid')->getData(),
                    (string) $form->get('email')->getData(),
                    (string) $form->get('first_name')->getData(),
                    (string) $form->get('last_name')->getData(),
                    new EncodedPassword((string) $form->get('password')->getData())
                );
            }
        ]);
    }
}
