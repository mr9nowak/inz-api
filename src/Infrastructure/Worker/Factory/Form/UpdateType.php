<?php

declare(strict_types=1);

namespace App\Infrastructure\Worker\Factory\Form;

use App\Domain\Worker\Model\Worker;
use App\Infrastructure\Common\Factory\AbstractFactory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class UpdateType
 *
 * @package App\Infrastructure\Worker\Factory\Form
 */
class UpdateType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('uuid', null, [
                'mapped' => false,
            ])
            ->add('first_name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'worker.exception.first_name.not_blank',
                    ]),
                ],
                'mapped' => false,
            ])
            ->add('last_name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'worker.exception.last_name.not_blank',
                    ]),
                ],
                'mapped' => false,
            ])
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Worker::class,
            'csrf_protection' => false,
            'method' => AbstractFactory::UPDATE,
        ]);
    }
}
