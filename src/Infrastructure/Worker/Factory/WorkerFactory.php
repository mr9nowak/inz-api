<?php

declare(strict_types=1);

namespace App\Infrastructure\Worker\Factory;

use App\Domain\Worker\Factory\WorkerFactoryInterface;
use App\Domain\Worker\Model\Worker;
use App\Infrastructure\Common\Factory\AbstractFactory;
use App\Infrastructure\Worker\Factory\Form\CreateType;
use App\Infrastructure\Worker\Factory\Form\UpdateType;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Class WorkerFactory
 *
 * @package App\Infrastructure\Worker\Factory
 */
class WorkerFactory extends AbstractFactory implements WorkerFactoryInterface
{
    /**
     * WorkerFactory constructor.
     *
     * @param FormFactoryInterface $factory
     */
    public function __construct(FormFactoryInterface $factory)
    {
        parent::__construct($factory);
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $data): Worker
    {
        $this->formClass = CreateType::class;

        return $this->execute(self::CREATE, $data);
    }

    public function update(array $data, $object): Worker
    {
        $this->formClass = UpdateType::class;

        return $this->execute(self::UPDATE, $data, $object);
    }
}
