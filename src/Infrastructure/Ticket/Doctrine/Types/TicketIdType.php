<?php

declare(strict_types=1);

namespace App\Infrastructure\Ticket\Doctrine\Types;

use App\Domain\Common\ValueObject\AggregateRootId;
use App\Domain\Ticket\ValueObject\TicketId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Ramsey\Uuid\Doctrine\UuidBinaryType;

/**
 * Class TicketIdType
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Infrastructure\Ticket\Doctrine\Types
 */
class TicketIdType extends UuidBinaryType
{
    const TICKET_ID = 'ticketId';

    /**
     * {@inheritDoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return (null === $value) ? null : TicketId::fromBytes($value);
    }

    /**
     * {@inheritDoc}
     *
     * @param AggregateRootId|null $value
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return (null === $value) ? null : $value->bytes();
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return self::TICKET_ID;
    }
}
