<?php

declare(strict_types=1);

namespace App\Infrastructure\Ticket\Repository;

use App\Domain\Ticket\Exception\TicketNotFoundException;
use App\Domain\Ticket\Model\Ticket;
use App\Domain\Ticket\Repository\TicketRepositoryInterface;
use App\Domain\Ticket\ValueObject\TicketId;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class TicketRepository
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Infrastructure\Ticket\Repository
 */
class TicketRepository extends ServiceEntityRepository implements TicketRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ticket::class);
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return $this->createQueryBuilder('ticket')
            ->orderBy('ticket.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * {@inheritDoc}
     */
    public function findOneByUuid(TicketId $ticketId): ?Ticket
    {
        return $this->createQueryBuilder('ticket')
            ->where('ticket.uuid = :uuid')
            ->setParameter('uuid', $ticketId->bytes())
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * {@inheritDoc}
     */
    public function getListAnswersAndAssignedWorkerWithHeader(): array
    {
        $result = $this->createQueryBuilder('t')
            ->select(implode(',', [
                't.answer1',
                't.answer2',
                't.answer3',
                't.answer4',
                't.answer5',
                't.answer6',
                'w.uuid',
            ]))
            ->innerJoin('t.worker', 'w')
            ->getQuery()
            ->getResult();
        $result = array_map(function ($row) {
            $row['uuid'] = (string) $row['uuid'];

            return array_values($row);
        }, $result);
        $header = ['answer1', 'answer2', 'answer3', 'answer4', 'answer5', 'answer6', 'worker'];
        array_unshift($result, $header);

        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function getOneByUuid(TicketId $ticketId): Ticket
    {
        $ticket = $this->findOneByUuid($ticketId);

        if (! $ticket) {
            throw new TicketNotFoundException();
        }

        return $ticket;
    }

    /**
     * {@inheritDoc}
     */
    public function save(Ticket $ticket): void
    {
        $this->_em->persist($ticket);
        $this->_em->flush();
    }
}
