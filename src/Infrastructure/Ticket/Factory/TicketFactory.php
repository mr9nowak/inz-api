<?php

declare(strict_types=1);

namespace App\Infrastructure\Ticket\Factory;

use App\Domain\Ticket\Factory\TicketFactoryInterface;
use App\Domain\Ticket\Model\Ticket;
use App\Infrastructure\Common\Factory\AbstractFactory;
use App\Infrastructure\Ticket\Factory\Form\CreateType;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Class TicketFactory
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Infrastructure\Ticket\Factory
 */
class TicketFactory extends AbstractFactory implements TicketFactoryInterface
{
    public function __construct(FormFactoryInterface $formFactory)
    {
        parent::__construct($formFactory);
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $data): Ticket
    {
        $this->formClass = CreateType::class;

        return $this->execute(self::CREATE, $data);
    }
}
