<?php

declare(strict_types=1);

namespace App\Infrastructure\Ticket\Factory\Form;

use App\Domain\Ticket\Model\Ticket;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class CreateType
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Infrastructure\Ticket\Factory\Form
 */
class CreateType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('uuid', null, [
                'mapped' => false,
            ])
            ->add('email', EmailType::class, [
                'constraints' => [
                    new Email([
                        'message' => 'ticket.exception.email.not_valid',
                    ]),
                    new NotBlank([
                        'message' => 'ticket.exception.email.not_blank',
                    ]),
                ],
                'mapped' => false,
            ])
            ->add('content', TextareaType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'ticket.exception.content.not_blank',
                    ]),
                ],
                'mapped' => false,
            ])
            ->add('answer1', ChoiceType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'ticket.exception.answer1.not_blank',
                    ]),
                ],
                'choices' => [
                    'content' => 'content',
                    'action' => 'action',
                    'expansion' => 'expansion',
                ],
                'mapped' => false,
            ])
            ->add('answer2', ChoiceType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'ticket.exception.answer2.not_blank',
                    ]),
                ],
                'choices' => [
                    'yes' => 'yes',
                    'no' => 'no',
                ],
                'mapped' => false,
            ])
            ->add('answer3', ChoiceType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'ticket.exception.answer3.not_blank',
                    ]),
                ],
                'choices' => [
                    'yes' => 'yes',
                    'no' => 'no',
                ],
                'mapped' => false,
            ])
            ->add('answer4', ChoiceType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'ticket.exception.answer4.not_blank',
                    ]),
                ],
                'choices' => [
                    'chrome' => 'chrome',
                    'firefox' => 'firefox',
                    'opera' => 'opera',
                    'ie' => 'ie',
                    'edge' => 'edge',
                    'safari' => 'safari',
                    'other' => 'other',
                ],
                'mapped' => false,
            ])
            ->add('answer5', ChoiceType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'ticket.exception.answer5.not_blank',
                    ]),
                ],
                'choices' => [
                    'pc' => 'pc',
                    'tablet' => 'tablet',
                    'phone' => 'phone',
                ],
                'mapped' => false,
            ])
            ->add('answer6', ChoiceType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'ticket.exception.answer6.not_blank',
                    ]),
                ],
                'choices' => [
                    'absent' => 'absent',
                    'disposable' => 'disposable',
                    'recurrent' => 'recurrent',
                ],
                'mapped' => false,
            ])
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Ticket::class,
            'empty_data' => function (FormInterface $form) {
                return Ticket::create(
                    $form->get('uuid')->getData(),
                    (string) $form->get('email')->getData(),
                    (string) $form->get('content')->getData(),
                    (string) $form->get('answer1')->getData(),
                    (string) $form->get('answer2')->getData(),
                    (string) $form->get('answer3')->getData(),
                    (string) $form->get('answer4')->getData(),
                    (string) $form->get('answer5')->getData(),
                    (string) $form->get('answer6')->getData()
                );
            },
        ]);
    }
}
