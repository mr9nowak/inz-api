<?php

declare(strict_types=1);

namespace App\Domain\Ticket\Exception;

use InvalidArgumentException;

/**
 * Class MinEstimateTimeException
 *
 * @package App\Domain\Ticket\Exception
 */
class MinEstimateTimeException extends InvalidArgumentException
{
    /**
     * MinEstimateTimeException constructor.
     */
    public function __construct()
    {
        parent::__construct('ticket.exception.estimate_time_must_be_higher_than_0', 77000);
    }
}
