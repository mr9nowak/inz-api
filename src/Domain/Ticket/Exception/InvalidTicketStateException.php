<?php

declare(strict_types=1);

namespace App\Domain\Ticket\Exception;

use InvalidArgumentException;

/**
 * Class InvalidTicketStateException
 *
 * @package App\Domain\Ticket\Exception
 */
class InvalidTicketStateException extends InvalidArgumentException
{
    /**
     * InvalidTicketStateException constructor.
     */
    public function __construct()
    {
        parent::__construct('ticket.exception.invalid_state', 77000);
    }
}
