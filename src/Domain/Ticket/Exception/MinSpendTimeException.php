<?php

declare(strict_types=1);

namespace App\Domain\Ticket\Exception;

use InvalidArgumentException;

/**
 * Class MinSpendTimeException
 *
 * @package App\Domain\Ticket\Exception
 */
class MinSpendTimeException extends InvalidArgumentException
{
    /**
     * MinSpendTimeException constructor.
     */
    public function __construct()
    {
        parent::__construct('ticket.exception.spend_time_must_be_higher_than_0', 77000);
    }
}
