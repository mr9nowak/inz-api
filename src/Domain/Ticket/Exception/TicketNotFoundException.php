<?php

declare(strict_types=1);

namespace App\Domain\Ticket\Exception;

use App\Domain\Common\Exception\NotFoundException;

/**
 * Class TicketNotFoundException
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub
 * @package App\Domain\Ticket\Exception
 */
class TicketNotFoundException extends NotFoundException
{
    /**
     * TicketNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('ticket.exception.not_found', 404);
    }
}
