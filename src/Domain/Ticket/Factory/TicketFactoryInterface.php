<?php

declare(strict_types=1);

namespace App\Domain\Ticket\Factory;

use App\Domain\Ticket\Model\Ticket;

/**
 * Interface TicketFactoryInterface
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Domain\Ticket\Factory
 */
interface TicketFactoryInterface
{
    /**
     * Create.
     *
     * @param array $data
     * @return Ticket
     */
    public function create(array $data): Ticket;
}
