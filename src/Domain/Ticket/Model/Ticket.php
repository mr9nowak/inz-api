<?php

declare(strict_types=1);

namespace App\Domain\Ticket\Model;

use App\Domain\Common\ValueObject\AggregateRoot;
use App\Domain\Ticket\ValueObject\TicketId;
use App\Domain\Ticket\ValueObject\TicketState;
use App\Domain\Worker\Model\Worker;
use DateTime;
use DateTimeInterface;

/**
 * Class Ticket
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Domain\Ticket\Model
 */
class Ticket extends AggregateRoot
{
    /**
     * @var TicketId
     */
    protected $uuid;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $answer1;

    /**
     * @var string
     */
    private $answer2;

    /**
     * @var string
     */
    private $answer3;

    /**
     * @var string
     */
    private $answer4;

    /**
     * @var string
     */
    private $answer5;

    /**
     * @var string
     */
    private $answer6;

    /**
     * @var float|null
     */
    private $estimateTime;

    /**
     * @var float|null
     */
    private $spendTime;

    /**
     * @var string
     */
    private $state = TicketState::OPENED;

    /**
     * @var Worker|null
     */
    private $worker;

    /**
     * @var DateTimeInterface
     */
    private $createdAt;

    // -------------------------------------------------------------------------

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getAnswer1(): string
    {
        return $this->answer1;
    }

    /**
     * @return string
     */
    public function getAnswer2(): string
    {
        return $this->answer2;
    }

    /**
     * @return string
     */
    public function getAnswer3(): string
    {
        return $this->answer3;
    }

    /**
     * @return string
     */
    public function getAnswer4(): string
    {
        return $this->answer4;
    }

    /**
     * @return string
     */
    public function getAnswer5(): string
    {
        return $this->answer5;
    }

    /**
     * @return string
     */
    public function getAnswer6(): string
    {
        return $this->answer6;
    }

    /**
     * @return TicketId
     */
    public function getUuid(): TicketId
    {
        return $this->uuid;
    }

    /**
     * @return float|null
     */
    public function getEstimateTime(): ?float
    {
        return $this->estimateTime;
    }

    /**
     * @return float|null
     */
    public function getSpendTime(): ?float
    {
        return $this->spendTime;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @return Worker|null
     */
    public function getWorker(): ?Worker
    {
        return $this->worker;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    // -------------------------------------------------------------------------

    /**
     * Ticket constructor.
     *
     * @param TicketId $ticketId
     * @param string $email
     * @param string $content
     * @param string $answer1
     * @param string $answer2
     * @param string $answer3
     * @param string $answer4
     * @param string $answer5
     * @param string $answer6
     */
    public function __construct(
        TicketId $ticketId,
        string $email,
        string $content,
        string $answer1,
        string $answer2,
        string $answer3,
        string $answer4,
        string $answer5,
        string $answer6
    ) {
        parent::__construct($ticketId);

        $this->createdAt = new DateTime();
        $this->email = $email;
        $this->content = $content;
        $this->answer1 = $answer1;
        $this->answer2 = $answer2;
        $this->answer3 = $answer3;
        $this->answer4 = $answer4;
        $this->answer5 = $answer5;
        $this->answer6 = $answer6;
    }

    /**
     * Create.
     *
     * @param TicketId $ticketId
     * @param string $email
     * @param string $content
     * @param string $answer1
     * @param string $answer2
     * @param string $answer3
     * @param string $answer4
     * @param string $answer5
     * @param string $answer6
     * @return static
     */
    public static function create(
        TicketId $ticketId,
        string $email,
        string $content,
        string $answer1,
        string $answer2,
        string $answer3,
        string $answer4,
        string $answer5,
        string $answer6
    ): self {
        return new self(
            $ticketId,
            $email,
            $content,
            $answer1,
            $answer2,
            $answer3,
            $answer4,
            $answer5,
            $answer6
        );
    }

    /**
     * @param Worker $worker
     * @return $this
     */
    public function assignWorker(Worker $worker): self
    {
        $this->worker = $worker;

        return $this;
    }

    /**
     * @param float $estimateTime
     * @return Ticket
     */
    public function setEstimateTime(float $estimateTime): Ticket
    {
        $this->estimateTime = $estimateTime;

        return $this;
    }

    /**
     * @param float $spendTime
     * @return Ticket
     */
    public function setSpendTime(float $spendTime): Ticket
    {
        $this->spendTime = $spendTime;

        return $this;
    }

    /**
     * @param string $state
     * @return Ticket
     */
    public function setState(string $state): Ticket
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return array
     */
    public function toArrayForAutoAssign(): array
    {
        return [
            'answer1' => $this->answer1,
            'answer2' => $this->answer2,
            'answer3' => $this->answer3,
            'answer4' => $this->answer4,
            'answer5' => $this->answer5,
            'answer6' => $this->answer6,
        ];
    }
}
