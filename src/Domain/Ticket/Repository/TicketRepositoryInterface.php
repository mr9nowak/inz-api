<?php

declare(strict_types=1);

namespace App\Domain\Ticket\Repository;

use App\Domain\Ticket\Exception\TicketNotFoundException;
use App\Domain\Ticket\Model\Ticket;
use App\Domain\Ticket\ValueObject\TicketId;

/**
 * Interface TicketRepositoryInterface
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Domain\Ticket\Repository
 */
interface TicketRepositoryInterface
{
    /**
     * Find all.
     *
     * @return Ticket[]
     */
    public function findAll();

    /**
     * Find one by uuid.
     *
     * @param TicketId $ticketId
     * @return Ticket|null
     */
    public function findOneByUuid(TicketId $ticketId): ?Ticket;

    /**
     * List of answers and assigned workers with header.
     *
     * @return array
     */
    public function getListAnswersAndAssignedWorkerWithHeader(): array;

    /**
     * Get one by uuid.
     *
     * @param TicketId $ticketId
     * @return Ticket
     * @throws TicketNotFoundException
     */
    public function getOneByUuid(TicketId $ticketId): Ticket;

    /**
     * Save.
     *
     * @param Ticket $ticket
     */
    public function save(Ticket $ticket): void;
}
