<?php

declare(strict_types=1);

namespace App\Domain\Ticket\ValueObject;

use App\Domain\Common\ValueObject\AggregateRootId;

/**
 * Class TicketId
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Domain\Ticket\ValueObject
 */
class TicketId extends AggregateRootId
{
}
