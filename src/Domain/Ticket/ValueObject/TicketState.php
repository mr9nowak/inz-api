<?php

declare(strict_types=1);

namespace App\Domain\Ticket\ValueObject;

use App\Domain\Ticket\Exception\InvalidTicketStateException;

/**
 * Class TicketState
 *
 * @package App\Domain\Ticket\ValueObject
 */
class TicketState
{
    const OPENED = 'opened';
    const CLOSED = 'closed';

    /**
     * @var string
     */
    private $state;

    /**
     * TicketState constructor.
     *
     * @param string $state
     */
    public function __construct(string $state)
    {
        $this->setState($state);
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        if (! self::isValid($state)) {
            throw new InvalidTicketStateException();
        }

        $this->state = $state;
    }

    /**
     * @return array
     */
    public static function states(): array
    {
        return [
            self::OPENED,
            self::CLOSED,
        ];
    }

    /**
     * @param string $state
     * @return bool
     */
    public static function isValid(string $state): bool
    {
        return in_array($state, self::states());
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->state;
    }
}
