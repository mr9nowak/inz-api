<?php

namespace App\Domain\Lem2;

/**
 * Class Lem2
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Domain\Lem2
 */
class Lem2
{
    public $blocks;
    public $dataset = [];
    public $datasetConcepts;
    public $singleLocalCovering;

    public $goal;
    public $concept;

    public function newConcept()
    {
        $this->datasetConcepts = [];

        $decisionIndex = count($this->dataset[0]) - 1;
        $dataset = array_slice($this->dataset, 0);
        $decision = $dataset[0][$decisionIndex];

        array_shift($dataset);

        $column = array_map(function ($value) use ($decisionIndex) {
            return $value[$decisionIndex];
        }, $dataset);

        $decisionValues = array_unique($column);


        foreach ($decisionValues as $decisionValue) {
            $cases = [];

            foreach ($column as $index => $value) {
                if ($decisionValue === $value) {
                    $cases[] = $index + 1;
                }
            }

            $concept = [
                'decision' => $decision,
                'value' => $decisionValue,
                'cases' => $cases,
            ];

            $this->datasetConcepts[] = $concept;
        }
    }

    public function newAttributeValueBlocks()
    {
        $this->blocks = [];
        $dataset = [];

        foreach ($this->dataset as $index => $item) {
            $dataset[$index] = array_slice($item, 0);
        }

        $attributeNames = array_slice($dataset[0], 0);
        // Remove decision label
        array_pop($attributeNames);
        // Remove labels
        array_shift($dataset);

        foreach ($attributeNames as $index => $attributeName) {
            $this->blocks[$attributeName] = [];

            $column = array_map(function ($value) use ($index) {
                return $value[$index];
            }, $dataset);

            $attributeValues = [];

            foreach ($column as $attributeValue) {
                if (array_search($attributeValue, $attributeValues) !== false) {
                    continue;
                }

                $attributeValues[] = $attributeValue;
            }

            foreach ($attributeValues as $attributeValue) {
                $returnAttributeValues = [];

                foreach ($column as $index => $value) {
                    if ($value == $attributeValue) {
                        $returnAttributeValues[] = $index + 1;
                    }
                }

                $this->blocks[$attributeName][$attributeValue] = $returnAttributeValues;
            }
        }
    }

    public function initialize($dataset)
    {
        $this->dataset = $dataset;
        $this->newConcept();
        $this->newAttributeValueBlocks();
    }

    public function initializeProcedure($concept)
    {
        $this->concept = $concept;
        $this->singleLocalCovering = [];
        $this->goal = $concept['cases'];
    }

    public function invokeProcedure($concept)
    {
        $this->initializeProcedure($concept);
        $this->newRuleset();
    }

    public function newRuleset()
    {
        $casesCoveredByRuleset = [];

        while (count($this->goal)) {
            $rule = $this->newRule();
            $this->singleLocalCovering[] = $rule;
            $casesCoveredByRuleset = array_unique(array_merge($casesCoveredByRuleset, $rule['coveredCases']));
            sort($casesCoveredByRuleset);
            $this->goal = array_diff($this->concept['cases'], $casesCoveredByRuleset);
        }
    }

    public function newRule()
    {
        $rule = [
            'conditions' => [],
            'decision' => [
                'name' => $this->concept['decision'],
                'value' => $this->concept['value'],
            ],
            'coveredCases' => [],
            'consistent' => true,
        ];

        do {
            $intersections = $this->newGoalBlockIntersections($rule);

            if (count($intersections) === 0) {
                $rule['consistent'] = false;
                // Inconsistent Rule Created
                break;
            }

            $bestBlock = $this->selectBestBlock($intersections);
            $condition = [
                'attribute' => $bestBlock['attribute'],
                'value' => $bestBlock['value'],
            ];
            $rule['conditions'][] = $condition;
            $this->goal = array_intersect($this->goal, $bestBlock['cases']);
            $block = $this->blocks[$condition['attribute']][$condition['value']];

            if (count($rule['coveredCases']) === 0) {
                $rule['coveredCases'] = $block;
            } else {
                $rule['coveredCases'] = array_intersect($rule['coveredCases'], $block);
            }

            $isSubset = $this->isSuperset($this->concept['cases'], $rule['coveredCases']);
        } while (count($rule['conditions']) === 0 || !$isSubset);

        return $rule;
    }

    public function findCondition($rule, $targetCondition)
    {
        $targetIndex = -1;

        foreach ($rule['conditions'] as $conditionIndex => $condition) {
            if ($condition['attribute'] !== $targetCondition['attribute']) {
                continue;
            }

            if ($condition['value'] !== $targetCondition['value']) {
                continue;
            }

            $targetIndex = $conditionIndex;
        }

        return $targetIndex;
    }

    public function newGoalBlockIntersections($rule)
    {
        $intersections = [];

        foreach ($this->blocks as $attribute => $attributeBlocks) {
            foreach ($attributeBlocks as $attributeValue => $items) {
                $conditionIndex = $this->findCondition($rule, [
                    'attribute' => $attribute,
                    'value' => $attributeValue,
                ]);

                if ($conditionIndex !== -1) {
                    continue;
                }

                $blockCases = $this->blocks[$attribute][$attributeValue];
                $intersection = [
                    'attribute' => $attribute,
                    'value' => $attributeValue,
                    'cases' => array_intersect($blockCases, $this->goal)
                ];

                if (count($intersection['cases'])) {
                    $intersections[] = $intersection;
                }
            }
        }

        return $intersections;
    }

    public function selectBestBlock($intersections)
    {
        $bestBlock = $intersections[0];
        $bestCardinality = count($this->blocks[$intersections[0]['attribute']][$intersections[0]['value']]);

        foreach ($intersections as $intersection) {
            $cardinality = count($this->blocks[$intersection['attribute']][$intersection['value']]);

            if (count($intersection['cases']) < count($bestBlock['cases'])) {
                continue;
            }

            if (count($intersection['cases']) > count($bestBlock['cases'])) {
                $bestBlock = $intersection;
                $bestCardinality = $cardinality;
                continue;
            }

            if ($cardinality > $bestCardinality) {
                continue;
            }

            if ($cardinality < $bestCardinality) {
                $bestBlock = $intersection;
                $bestCardinality = $cardinality;
                continue;
            }
        }

        return $bestBlock;
    }

    private function isSuperset($a, $b)
    {
        foreach ($b as $elem) {
            if (array_search($elem, $a) === false) {
                return false;
            }
        }

        return true;
    }

    public function match($data = [])
    {
        foreach ($this->datasetConcepts as $datasetConcept) {
            $this->invokeProcedure($datasetConcept);

            foreach ($this->singleLocalCovering as $singleLocalCovering) {
                $match = true;

                foreach ($singleLocalCovering['conditions'] as $condition) {
                    if (isset($data[$condition['attribute']]) && $data[$condition['attribute']] == $condition['value']) {
                        $match = $match && true;
                    } else {
                        $match = $match && false;
                    }
                }

                if ($match) {
                    return $singleLocalCovering['decision'];
                }
            }
        }

        return null;
    }
}
