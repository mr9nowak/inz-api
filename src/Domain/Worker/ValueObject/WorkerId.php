<?php

declare(strict_types=1);

namespace App\Domain\Worker\ValueObject;

use App\Domain\Common\ValueObject\AggregateRootId;

/**
 * Class WorkerId
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Domain\Worker\ValueObject
 */
class WorkerId extends AggregateRootId
{
}
