<?php

declare(strict_types=1);

namespace App\Domain\Worker\Factory;

use App\Domain\Worker\Model\Worker;

/**
 * Interface WorkerFactoryInterface
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Domain\Worker\Factory
 */
interface WorkerFactoryInterface
{
    /**
     * Create.
     *
     * @param array $data
     * @return Worker
     */
    public function create(array $data): Worker;
}
