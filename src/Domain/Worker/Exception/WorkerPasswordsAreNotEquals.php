<?php

declare(strict_types=1);

namespace App\Domain\Worker\Exception;

use InvalidArgumentException;

/**
 * Class WorkerPasswordsAreNotEquals
 *
 * @package App\Domain\Worker\Exception
 */
class WorkerPasswordsAreNotEquals extends InvalidArgumentException
{
    public function __construct()
    {
        parent::__construct('worker.password.not.equals');
    }
}
