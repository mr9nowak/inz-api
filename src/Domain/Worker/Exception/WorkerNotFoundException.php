<?php

declare(strict_types=1);

namespace App\Domain\Worker\Exception;

use App\Domain\Common\Exception\NotFoundException;

/**
 * Class WorkerNotFoundException
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Domain\Worker\Exception
 */
class WorkerNotFoundException extends NotFoundException
{
    /**
     * WorkerNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('worker.exception.not_found', 404);
    }
}
