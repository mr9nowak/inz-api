<?php

declare(strict_types=1);

namespace App\Domain\Worker\Model;

use App\Domain\Common\ValueObject\AggregateRoot;
use App\Domain\Security\ValueObject\AuthWorker;
use App\Domain\Security\ValueObject\EncodedPasswordInterface;
use App\Domain\Worker\ValueObject\WorkerId;
use DateTime;
use DateTimeInterface;

/**
 * Class Worker
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Domain\Worker\Model
 */
class Worker extends AggregateRoot
{
    /**
     * @var WorkerId
     */
    protected $uuid;

    /**
     * @var AuthWorker
     */
    private $auth;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var DateTimeInterface
     */
    private $createdAt;

    /**
     * Worker constructor.
     *
     * @param WorkerId $workerId
     * @param string $email
     * @param string $firstName
     * @param string $lastName
     * @param EncodedPasswordInterface $encodedPassword
     * @throws \Exception
     */
    public function __construct(
        WorkerId $workerId,
        string $email,
        string $firstName,
        string $lastName,
        EncodedPasswordInterface $encodedPassword
    ) {
        parent::__construct($workerId);

        $this->auth = new AuthWorker($email, $encodedPassword);
        $this->createdAt = new DateTime();
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return AuthWorker
     */
    public function auth(): AuthWorker
    {
        return $this->auth;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->auth->username();
    }

    /**
     * Create.
     *
     * @param WorkerId $workerId
     * @param string $email
     * @param string $firstName
     * @param string $lastName
     * @param EncodedPasswordInterface $encodedPassword
     * @return Worker
     */
    public static function create(
        WorkerId $workerId,
        string $email,
        string $firstName,
        string $lastName,
        EncodedPasswordInterface $encodedPassword
    ): self {
        return new self($workerId, $email, $firstName, $lastName, $encodedPassword);
    }


    /**
     * @param string $firstName
     * @param string $lastName
     * @return Worker
     */
    public function update(string $firstName, string $lastName): self
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;

        return $this;
    }
}
