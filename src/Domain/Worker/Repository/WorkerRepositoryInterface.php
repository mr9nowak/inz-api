<?php

declare(strict_types=1);

namespace App\Domain\Worker\Repository;

use App\Domain\Worker\Exception\WorkerNotFoundException;
use App\Domain\Worker\Model\Worker;
use App\Domain\Worker\ValueObject\WorkerId;

/**
 * Interface WorkerRepositoryInterface
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Domain\Worker\Repository
 */
interface WorkerRepositoryInterface
{
    /**
     * Find all.
     *
     * @return Worker[]
     */
    public function findAll();

    public function findOneByUsername(?string $username): ?Worker;

    /**
     * Find one by uuid.
     *
     * @param WorkerId $workerId
     * @return Worker|null
     */
    public function findOneByUuid(WorkerId $workerId): ?Worker;

    /**
     * Get one by uuid.
     *
     * @param WorkerId $workerId
     * @return Worker
     * @throws WorkerNotFoundException
     */
    public function getOneByUuid(WorkerId $workerId): Worker;

    /**
     * Save.
     *
     * @param Worker $worker
     */
    public function save(Worker $worker): void;
}
