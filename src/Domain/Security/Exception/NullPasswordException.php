<?php

declare(strict_types=1);

namespace App\Domain\Security\Exception;

use InvalidArgumentException;

/**
 * Class NullPasswordException
 *
 * @package App\Domain\Security\Exception
 */
class NullPasswordException extends InvalidArgumentException
{
    /**
     * NullPasswordException constructor.
     */
    public function __construct()
    {
        parent::__construct("security.exception.null_password", 6006);
    }
}
