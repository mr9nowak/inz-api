<?php

declare(strict_types=1);

namespace App\Domain\Security\ValueObject;

/**
 * Interface EncodedPasswordInterface
 *
 * @package App\Domain\Security\ValueObject
 */
interface EncodedPasswordInterface
{
    /**
     * EncodedPasswordInterface constructor.
     *
     * @param string $plainPassword
     */
    public function __construct(string $plainPassword);

    /**
     * @param string $hash
     * @return bool
     */
    public function matchHash(string $hash): bool;

    /**
     * @return string
     */
    public function __toString(): string;
}
