<?php

declare(strict_types=1);

namespace App\Domain\Security\ValueObject;

use App\Domain\Worker\Exception\WorkerPasswordsAreNotEquals;

/**
 * Class AuthWorker
 *
 * @package App\Domain\Security\ValueObject
 */
final class AuthWorker
{
    const DEFAULT_ROLES = [
        'ROLE_USER'
    ];

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $passwordHash;

    /**
     * @var array
     */
    private $roles = [];

    /**
     * AuthWorker constructor.
     *
     * @param string $username
     * @param EncodedPasswordInterface $encodedPassword
     * @param array $roles
     */
    public function __construct(
        string $username,
        EncodedPasswordInterface $encodedPassword,
        array $roles = []
    ) {
        $this->username = $username;
        $this->passwordHash = (string) $encodedPassword;
        $this->roles = array_merge(self::DEFAULT_ROLES, $roles);
    }

    /**
     * @return string
     */
    public function username(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function password(): string
    {
        return $this->passwordHash;
    }

    /**
     * @return array
     */
    public function roles(): array
    {
        return $this->roles;
    }

    /**
     * @param EncodedPasswordInterface $oldPassword
     * @param EncodedPasswordInterface $newPassword
     */
    public function changePassword(EncodedPasswordInterface $oldPassword, EncodedPasswordInterface $newPassword): void
    {
        if (!$oldPassword->matchHash($this->passwordHash)) {

            throw new WorkerPasswordsAreNotEquals();
        }

        $this->passwordHash = (string) $newPassword;
    }
}
