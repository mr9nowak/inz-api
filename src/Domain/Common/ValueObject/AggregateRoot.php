<?php

declare(strict_types=1);

namespace App\Domain\Common\ValueObject;

/**
 * Class AggregateRoot
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Domain\Common\ValueObject
 */
abstract class AggregateRoot
{
    /**
     * @var AggregateRootId
     */
    protected $uuid;

    /**
     * AggregateRoot constructor.
     *
     * @param AggregateRootId $aggregateRootId
     */
    protected function __construct(AggregateRootId $aggregateRootId)
    {
        $this->uuid = $aggregateRootId;
    }

    /**
     * @return AggregateRootId
     */
    public function uuid(): AggregateRootId
    {
        return $this->uuid;
    }

    /**
     * @param AggregateRootId $aggregateRootId
     * @return bool
     */
    final public function equals(AggregateRootId $aggregateRootId): bool
    {
        return $this->uuid->equals($aggregateRootId);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->uuid;
    }
}
