<?php

declare(strict_types=1);

namespace App\Domain\Common\Exception;

use Exception;

/**
 * Class NotFoundException
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Domain\Common\Exception
 */
abstract class NotFoundException extends Exception
{
}
