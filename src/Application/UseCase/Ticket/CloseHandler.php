<?php

declare(strict_types=1);

namespace App\Application\UseCase\Ticket;

use App\Application\UseCase\Ticket\Request\Close;
use App\Domain\Ticket\Model\Ticket;
use App\Domain\Ticket\Repository\TicketRepositoryInterface;
use App\Domain\Ticket\ValueObject\TicketState;

/**
 * Class CloseHandler
 *
 * @package App\Application\UseCase\Ticket
 */
class CloseHandler
{
    /**
     * @var TicketRepositoryInterface
     */
    private $repository;

    /**
     * CloseHandler constructor.
     *
     * @param TicketRepositoryInterface $repository
     */
    public function __construct(TicketRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Close $request
     * @return Ticket
     */
    public function handle(Close $request): Ticket
    {
        $ticket = $this->repository->getOneByUuid($request->ticketId());
        $ticket->setState(TicketState::CLOSED);

        $this->repository->save($ticket);

        return $ticket;
    }
}
