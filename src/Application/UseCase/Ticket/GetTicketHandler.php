<?php

declare(strict_types=1);

namespace App\Application\UseCase\Ticket;

use App\Application\UseCase\Ticket\Request\GetTicket;
use App\Domain\Ticket\Exception\TicketNotFoundException;
use App\Domain\Ticket\Model\Ticket;
use App\Domain\Ticket\Repository\TicketRepositoryInterface;

/**
 * Class GetTicketHandler
 *
 * @package App\Application\UseCase\Ticket
 */
class GetTicketHandler
{
    /**
     * @var TicketRepositoryInterface
     */
    private $repository;

    /**
     * GetTicketHandler constructor.
     *
     * @param TicketRepositoryInterface $repository
     */
    public function __construct(TicketRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param GetTicket $request
     * @return Ticket
     * @throws TicketNotFoundException
     */
    public function handle(GetTicket $request): Ticket
    {
        return $this->repository->getOneByUuid($request->getUuid());
    }
}
