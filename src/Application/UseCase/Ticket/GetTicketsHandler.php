<?php

declare(strict_types=1);

namespace App\Application\UseCase\Ticket;

use App\Application\UseCase\Ticket\Request\GetTickets;
use App\Domain\Ticket\Repository\TicketRepositoryInterface;

/**
 * Class GetTicketsHandler
 *
 * @package App\Application\UseCase\Ticket
 */
class GetTicketsHandler
{
    /**
     * @var TicketRepositoryInterface
     */
    private $repository;

    /**
     * GetTicketsHandler constructor.
     *
     * @param TicketRepositoryInterface $repository
     */
    public function __construct(TicketRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param GetTickets $request
     * @return array
     */
    public function handle(GetTickets $request): array
    {
        return $this->repository->findAll();
    }
}
