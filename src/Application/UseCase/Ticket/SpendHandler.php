<?php

declare(strict_types=1);

namespace App\Application\UseCase\Ticket;

use App\Application\UseCase\Ticket\Request\Spend;
use App\Domain\Ticket\Model\Ticket;
use App\Domain\Ticket\Repository\TicketRepositoryInterface;

/**
 * Class SpendHandler
 *
 * @package App\Application\UseCase\Ticket
 */
class SpendHandler
{
    /**
     * @var TicketRepositoryInterface
     */
    private $repository;

    /**
     * SpendHandler constructor.
     *
     * @param TicketRepositoryInterface $repository
     */
    public function __construct(TicketRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Spend $request
     * @return Ticket
     */
    public function handle(Spend $request): Ticket
    {
        $ticket = $this->repository->getOneByUuid($request->ticketId());
        $ticket->setSpendTime($request->getSpend());

        $this->repository->save($ticket);

        return $ticket;
    }
}
