<?php

declare(strict_types=1);

namespace App\Application\UseCase\Ticket\Request;

use App\Domain\Ticket\ValueObject\TicketId;

/**
 * Class GetTicket
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Application\UseCase\Ticket\Request
 */
class GetTicket
{
    /**
     * @var TicketId
     */
    private $uuid;

    /**
     * GetTicket constructor.
     *
     * @param string $uuid
     */
    public function __construct(string $uuid)
    {
        $this->uuid = new TicketId($uuid);
    }

    /**
     * @return TicketId
     */
    public function getUuid(): TicketId
    {
        return $this->uuid;
    }
}
