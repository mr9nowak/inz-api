<?php

declare(strict_types=1);

namespace App\Application\UseCase\Ticket\Request;

use App\Domain\Ticket\Exception\MinEstimateTimeException;
use App\Domain\Ticket\ValueObject\TicketId;

/**
 * Class Estimate
 *
 * @package App\Application\UseCase\Ticket\Request
 */
class Estimate
{
    /**
     * @var TicketId
     */
    private $ticketId;

    /**
     * @var float
     */
    private $estimate;

    /**
     * Estimate constructor.
     *
     * @param TicketId $ticketId
     * @param float $estimate
     */
    public function __construct(TicketId $ticketId, float $estimate)
    {
        $this->ticketId = $ticketId;
        $this->setEstimate($estimate);
    }

    /**
     * @param float $estimate
     * @throws MinEstimateTimeException
     */
    protected function setEstimate(float $estimate)
    {
        if (0.0 >= $estimate) {
            throw new MinEstimateTimeException();
        }

        $this->estimate = $estimate;
    }

    /**
     * @return TicketId
     */
    public function ticketId(): TicketId
    {
        return $this->ticketId;
    }

    /**
     * @return float
     */
    public function getEstimate(): float
    {
        return $this->estimate;
    }
}
