<?php

declare(strict_types=1);

namespace App\Application\UseCase\Ticket\Request;

use App\Domain\Ticket\ValueObject\TicketId;

/**
 * Class Create
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Application\UseCase\Ticket\Request
 */
class Create
{
    /**
     * @var TicketId
     */
    private $ticketId;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $answer1;

    /**
     * @var string
     */
    private $answer2;

    /**
     * @var string
     */
    private $answer3;

    /**
     * @var string
     */
    private $answer4;

    /**
     * @var string
     */
    private $answer5;

    /**
     * @var string
     */
    private $answer6;

    /**
     * Create constructor.
     *
     * @param TicketId $ticketId
     * @param string $email
     * @param string $content
     * @param string $answer1
     * @param string $answer2
     * @param string $answer3
     * @param string $answer4
     * @param string $answer5
     * @param string $answer6
     */
    public function __construct(
        TicketId $ticketId,
        string $email,
        string $content,
        string $answer1,
        string $answer2,
        string $answer3,
        string $answer4,
        string $answer5,
        string $answer6
    ) {
        $this->ticketId = $ticketId;
        $this->email = $email;
        $this->content = $content;
        $this->answer1 = $answer1;
        $this->answer2 = $answer2;
        $this->answer3 = $answer3;
        $this->answer4 = $answer4;
        $this->answer5 = $answer5;
        $this->answer6 = $answer6;
    }

    /**
     * @return array
     */
    public function toForm(): array
    {
        return [
            'uuid' => $this->ticketId,
            'email' => $this->email,
            'content' => $this->content,
            'answer1' => $this->answer1,
            'answer2' => $this->answer2,
            'answer3' => $this->answer3,
            'answer4' => $this->answer4,
            'answer5' => $this->answer5,
            'answer6' => $this->answer6,
        ];
    }
}
