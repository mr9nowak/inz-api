<?php

declare(strict_types=1);

namespace App\Application\UseCase\Ticket\Request;

use App\Domain\Ticket\Exception\MinSpendTimeException;
use App\Domain\Ticket\ValueObject\TicketId;

/**
 * Class Spend
 *
 * @package App\Application\UseCase\Ticket\Request
 */
class Spend
{
    /**
     * @var TicketId
     */
    private $ticketId;

    /**
     * @var float
     */
    private $spend;

    /**
     * Spend constructor.
     *
     * @param TicketId $ticketId
     * @param float $spend
     */
    public function __construct(TicketId $ticketId, float $spend)
    {
        $this->ticketId = $ticketId;
        $this->setSpend($spend);
    }

    /**
     * @param float $spend
     * @throws MinSpendTimeException
     */
    protected function setSpend(float $spend)
    {
        if (0.0 >= $spend) {
            throw new MinSpendTimeException();
        }

        $this->spend = $spend;
    }

    /**
     * @return TicketId
     */
    public function ticketId(): TicketId
    {
        return $this->ticketId;
    }

    /**
     * @return float
     */
    public function getSpend(): float
    {
        return $this->spend;
    }
}
