<?php

declare(strict_types=1);

namespace App\Application\UseCase\Ticket\Request;

/**
 * Class GetTickets
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Application\UseCase\Ticket\Request
 */
class GetTickets
{
}
