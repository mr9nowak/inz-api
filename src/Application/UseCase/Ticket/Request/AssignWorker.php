<?php

declare(strict_types=1);

namespace App\Application\UseCase\Ticket\Request;

use App\Domain\Ticket\ValueObject\TicketId;
use App\Domain\Worker\ValueObject\WorkerId;

/**
 * Class AssignWorker
 *
 * @package App\Application\UseCase\Ticket\Request
 */
class AssignWorker
{
    /**
     * @var TicketId
     */
    private $ticketId;

    /**
     * @var WorkerId
     */
    private $workerId;

    /**
     * Estimate constructor.
     *
     * @param TicketId $ticketId
     * @param WorkerId $workerId
     */
    public function __construct(TicketId $ticketId, WorkerId $workerId)
    {
        $this->ticketId = $ticketId;
        $this->workerId = $workerId;
    }

    /**
     * @return TicketId
     */
    public function ticketId(): TicketId
    {
        return $this->ticketId;
    }

    public function workerId(): WorkerId
    {
        return $this->workerId;
    }
}
