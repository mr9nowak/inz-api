<?php

declare(strict_types=1);

namespace App\Application\UseCase\Ticket\Request;

use App\Domain\Ticket\ValueObject\TicketId;

/**
 * Class Close
 *
 * @package App\Application\UseCase\Ticket\Request
 */
class Close
{
    /**
     * @var TicketId
     */
    private $ticketId;

    /**
     * Close constructor.
     *
     * @param TicketId $ticketId
     */
    public function __construct(TicketId $ticketId)
    {
        $this->ticketId = $ticketId;
    }

    /**
     * @return TicketId
     */
    public function ticketId(): TicketId
    {
        return $this->ticketId;
    }
}
