<?php

declare(strict_types=1);

namespace App\Application\UseCase\Ticket;

use App\Application\UseCase\Ticket\Request\Create;
use App\Domain\Lem2\Lem2;
use App\Domain\Ticket\Factory\TicketFactoryInterface;
use App\Domain\Ticket\Model\Ticket;
use App\Domain\Ticket\Repository\TicketRepositoryInterface;
use App\Domain\Worker\Repository\WorkerRepositoryInterface;
use App\Domain\Worker\ValueObject\WorkerId;

/**
 * Class CreateHandler
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Application\UseCase\Ticket
 */
class CreateHandler
{
    /**
     * @var TicketFactoryInterface
     */
    private $factory;

    /**
     * @var Lem2
     */
    private $lem2;

    /**
     * @var TicketRepositoryInterface
     */
    private $repository;

    /**
     * @var WorkerRepositoryInterface
     */
    private $workerRepository;

    /**
     * CreateHandler constructor.
     *
     * @param Lem2 $lem2
     * @param TicketFactoryInterface $factory
     * @param TicketRepositoryInterface $repository
     * @param WorkerRepositoryInterface $workerRepository
     */
    public function __construct(
        Lem2 $lem2,
        TicketFactoryInterface $factory,
        TicketRepositoryInterface $repository,
        WorkerRepositoryInterface $workerRepository
    ) {
        $this->factory = $factory;
        $this->lem2 = $lem2;
        $this->repository = $repository;
        $this->workerRepository = $workerRepository;
    }

    /**
     * @param Create $request
     * @return Ticket
     */
    public function handle(Create $request): Ticket
    {
        $ticket = $this->factory->create($request->toForm());

        $this->lem2->initialize($this->repository->getListAnswersAndAssignedWorkerWithHeader());

        if ($result = $this->lem2->match($ticket->toArrayForAutoAssign())) {
            $ticket->assignWorker($this->workerRepository->getOneByUuid(new WorkerId($result['value'])));
        }

        $this->repository->save($ticket);

        return $ticket;
    }
}
