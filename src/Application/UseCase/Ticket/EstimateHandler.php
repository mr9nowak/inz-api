<?php

declare(strict_types=1);

namespace App\Application\UseCase\Ticket;

use App\Application\UseCase\Ticket\Request\Estimate;
use App\Domain\Ticket\Model\Ticket;
use App\Domain\Ticket\Repository\TicketRepositoryInterface;

/**
 * Class EstimateHandler
 *
 * @package App\Application\UseCase\Ticket
 */
class EstimateHandler
{
    /**
     * @var TicketRepositoryInterface
     */
    private $repository;

    /**
     * EstimateHandler constructor.
     *
     * @param TicketRepositoryInterface $repository
     */
    public function __construct(TicketRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Estimate $request
     * @return Ticket
     */
    public function handle(Estimate $request): Ticket
    {
        $ticket = $this->repository->getOneByUuid($request->ticketId());
        $ticket->setEstimateTime($request->getEstimate());

        $this->repository->save($ticket);

        return $ticket;
    }
}
