<?php

declare(strict_types=1);

namespace App\Application\UseCase\Ticket;

use App\Application\UseCase\Ticket\Request\AssignWorker;
use App\Application\UseCase\Ticket\Request\GetTicket;
use App\Application\UseCase\Worker\GetWorkerHandler;
use App\Application\UseCase\Worker\Request\GetWorker;
use App\Domain\Ticket\Model\Ticket;
use App\Domain\Ticket\Repository\TicketRepositoryInterface;

/**
 * Class AssignWorkerHandler
 *
 * @package App\Application\UseCase\Ticket
 */
class AssignWorkerHandler
{
    /**
     * @var TicketRepositoryInterface
     */
    private $repository;

    /**
     * @var GetTicketHandler
     */
    private $ticketQuery;

    /**
     * @var GetWorkerHandler
     */
    private $workerQuery;

    /**
     * AssignWorkerHandler constructor.
     *
     * @param TicketRepositoryInterface $repository
     * @param GetTicketHandler $ticketQuery
     * @param GetWorkerHandler $workerQuery
     */
    public function __construct(
        TicketRepositoryInterface $repository,
        GetTicketHandler $ticketQuery,
        GetWorkerHandler $workerQuery
    ) {
        $this->repository = $repository;
        $this->ticketQuery = $ticketQuery;
        $this->workerQuery = $workerQuery;
    }

    /**
     * @param AssignWorker $request
     * @return Ticket
     */
    public function handle(AssignWorker $request): Ticket
    {
        $ticket = $this->ticketQuery->handle(new GetTicket($request->ticketId()->__toString()));
        $worker = $this->workerQuery->handle(new GetWorker($request->workerId()->__toString()));

        $ticket->assignWorker($worker);

        $this->repository->save($ticket);

        return $ticket;
    }
}
