<?php

declare(strict_types=1);

namespace App\Application\UseCase\Security;

use App\Application\UseCase\Security\Request\Login;
use App\Domain\Security\Exception\AuthenticationException;
use App\Domain\Worker\Repository\WorkerRepositoryInterface;
use App\Infrastructure\Security\Model\Auth;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class LoginHandler
 *
 * @package App\Application\UseCase\Security
 */
class LoginHandler
{
    /**
     * @var AuthenticationUtils
     */
    private $authenticationUtils;

    /**
     * @var WorkerRepositoryInterface
     */
    private $workerRepository;

    /**
     * @var EncoderFactoryInterface
     */
    private $encoderFactory;

    /**
     * @var JWTTokenManagerInterface
     */
    private $tokenManager;

    /**
     * LoginHandler constructor.
     *
     * @param AuthenticationUtils $authenticationUtils
     * @param WorkerRepositoryInterface $workerRepository
     * @param EncoderFactoryInterface $encoderFactory
     * @param JWTTokenManagerInterface $tokenManager
     */
    public function __construct(
        AuthenticationUtils $authenticationUtils,
        WorkerRepositoryInterface $workerRepository,
        EncoderFactoryInterface $encoderFactory,
        JWTTokenManagerInterface $tokenManager
    ) {
        $this->authenticationUtils = $authenticationUtils;
        $this->workerRepository = $workerRepository;
        $this->encoderFactory = $encoderFactory;
        $this->tokenManager = $tokenManager;
    }

    /**
     * @param Login $request
     * @return string
     * @throws AuthenticationException
     */
    public function handle(Login $request): string
    {
        $worker = $this->workerRepository->findOneByUsername($request->username());

        if (! $worker) {
            throw new AuthenticationException();
        }

        $authWorker = new Auth(
            (string) $worker->uuid(),
            $worker->auth()
        );

        $encoder = $this->encoderFactory->getEncoder($authWorker);

        if (! $encoder->isPasswordValid($authWorker->getPassword(), $request->plainPassword(), $authWorker->getSalt())) {
            throw new AuthenticationException();
        }

        return $this->tokenManager->create($authWorker);
    }
}
