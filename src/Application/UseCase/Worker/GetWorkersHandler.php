<?php

declare(strict_types=1);

namespace App\Application\UseCase\Worker;

use App\Application\UseCase\Worker\Request\GetWorkers;
use App\Domain\Worker\Repository\WorkerRepositoryInterface;

/**
 * Class GetWorkersHandler
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Application\UseCase\Worker
 */
class GetWorkersHandler
{
    /**
     * @var WorkerRepositoryInterface
     */
    private $repository;

    /**
     * GetWorkersHandler constructor.
     *
     * @param WorkerRepositoryInterface $repository
     */
    public function __construct(WorkerRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param GetWorkers $request
     * @return array
     */
    public function handle(GetWorkers $request): array
    {
        return $this->repository->findAll();
    }
}
