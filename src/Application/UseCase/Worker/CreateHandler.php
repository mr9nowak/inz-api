<?php

declare(strict_types=1);

namespace App\Application\UseCase\Worker;

use App\Application\UseCase\Worker\Request\Create;
use App\Domain\Worker\Factory\WorkerFactoryInterface;
use App\Domain\Worker\Model\Worker;
use App\Domain\Worker\Repository\WorkerRepositoryInterface;

/**
 * Class CreateHandler
 *
 * @package App\Application\UseCase\Worker
 */
class CreateHandler
{
    /**
     * @var WorkerFactoryInterface
     */
    private $factory;

    /**
     * @var WorkerRepositoryInterface
     */
    private $repository;

    /**
     * GetWorkerHandler constructor.
     *
     * @param WorkerFactoryInterface $factory
     * @param WorkerRepositoryInterface $repository
     */
    public function __construct(
        WorkerFactoryInterface $factory,
        WorkerRepositoryInterface $repository
    ) {
        $this->factory = $factory;
        $this->repository = $repository;
    }

    /**
     * @param Create $request
     * @return Worker
     */
    public function handle(Create $request): Worker
    {
        $worker = $this->factory->create($request->toForm());
        $this->repository->save($worker);

        return $worker;
    }
}
