<?php

declare(strict_types=1);

namespace App\Application\UseCase\Worker;

use App\Application\UseCase\Worker\Request\Update;
use App\Domain\Worker\Factory\WorkerFactoryInterface;
use App\Domain\Worker\Model\Worker;
use App\Domain\Worker\Repository\WorkerRepositoryInterface;

/**
 * Class UpdateHandler
 *
 * @package App\Application\UseCase\Worker
 */
class UpdateHandler
{
    /**
     * @var WorkerFactoryInterface
     */
    private $factory;

    /**
     * @var WorkerRepositoryInterface
     */
    private $repository;

    /**
     * GetWorkerHandler constructor.
     *
     * @param WorkerFactoryInterface $factory
     * @param WorkerRepositoryInterface $repository
     */
    public function __construct(
        WorkerFactoryInterface $factory,
        WorkerRepositoryInterface $repository
    ) {
        $this->factory = $factory;
        $this->repository = $repository;
    }

    /**
     * @param Update $request
     * @return Worker
     */
    public function handle(Update $request): Worker
    {
        $worker = $this->repository->getOneByUuid($request->workerId());
        $worker = $this->factory->update($request->toForm(), $worker);
        $worker->update(
            $request->getFirstName(),
            $request->getLastName()
        );

        $this->repository->save($worker);

        return $worker;
    }
}
