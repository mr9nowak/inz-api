<?php

declare(strict_types=1);

namespace App\Application\UseCase\Worker;

use App\Application\UseCase\Worker\Request\GetWorker;
use App\Domain\Worker\Exception\WorkerNotFoundException;
use App\Domain\Worker\Model\Worker;
use App\Domain\Worker\Repository\WorkerRepositoryInterface;

/**
 * Class GetWorkerHandler
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Application\UseCase\Worker
 */
class GetWorkerHandler
{
    /**
     * @var WorkerRepositoryInterface
     */
    private $repository;

    /**
     * GetWorkerHandler constructor.
     *
     * @param WorkerRepositoryInterface $repository
     */
    public function __construct(WorkerRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param GetWorker $request
     * @return Worker
     * @throws WorkerNotFoundException
     */
    public function handle(GetWorker $request): Worker
    {
        return $this->repository->getOneByUuid($request->getUuid());
    }
}
