<?php

declare(strict_types=1);

namespace App\Application\UseCase\Worker\Request;

use App\Domain\Worker\ValueObject\WorkerId;

/**
 * Class GetWorker
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Application\UseCase\Worker\Request
 */
class GetWorker
{
    /**
     * @var WorkerId
     */
    private $uuid;

    /**
     * GetWorker constructor.
     *
     * @param string $uuid
     */
    public function __construct(string $uuid)
    {
        $this->uuid = new WorkerId($uuid);
    }

    /**
     * @return WorkerId
     */
    public function getUuid(): WorkerId
    {
        return $this->uuid;
    }
}
