<?php

declare(strict_types=1);

namespace App\Application\UseCase\Worker\Request;

use App\Domain\Worker\ValueObject\WorkerId;

/**
 * Class Update
 *
 * @package App\Application\UseCase\Worker\Request
 */
class Update
{
    /**
     * @var WorkerId
     */
    private $workerId;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * Create constructor.
     *
     * @param WorkerId $workerId
     * @param string $firstName
     * @param string $lastName
     */
    public function __construct(
        WorkerId $workerId,
        string $firstName,
        string $lastName
    ) {
        $this->workerId = $workerId;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * @return array
     */
    public function toForm(): array
    {
        return [
            'uuid' => $this->workerId,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
        ];
    }

    /**
     * @return WorkerId
     */
    public function workerId(): WorkerId
    {
        return $this->workerId;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }
}
