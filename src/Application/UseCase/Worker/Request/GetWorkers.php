<?php

declare(strict_types=1);

namespace App\Application\UseCase\Worker\Request;

/**
 * Class GetWorkers
 *
 * @author Jakub Nowak <mr9nowak@gmail.com>
 * @copyright 2019 Jakub Nowak
 * @package App\Application\UseCase\Worker\Request
 */
class GetWorkers
{
}
