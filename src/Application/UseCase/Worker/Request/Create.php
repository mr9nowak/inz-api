<?php

declare(strict_types=1);

namespace App\Application\UseCase\Worker\Request;

use App\Domain\Worker\ValueObject\WorkerId;

/**
 * Class Create
 *
 * @package App\Application\UseCase\Worker\Request
 */
class Create
{
    /**
     * @var WorkerId
     */
    private $workerId;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * Create constructor.
     *
     * @param WorkerId $workerId
     * @param string $email
     * @param string $firstName
     * @param string $lastName
     * @param string $plainPassword
     */
    public function __construct(
        WorkerId $workerId,
        string $email,
        string $firstName,
        string $lastName,
        string $plainPassword
    ) {
        $this->workerId = $workerId;
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return array
     */
    public function toForm(): array
    {
        return [
            'uuid' => $this->workerId,
            'email' => $this->email,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'password' => $this->plainPassword,
        ];
    }
}
